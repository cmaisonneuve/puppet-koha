# Deploy an entry in the Koha database passwd file
#
define koha::passwd (
  String $username,
  String $password,
  String $dbname,
  String $dbhost,
) {

  file_line { "koha::passwd for ${name}":
    path  => '/etc/passwd',
    match => "^${name}:",
    line  => "${name}:${username}:${password}:${dbname}:${dbhost}"
  }

}
