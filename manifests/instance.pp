# Deploy a Koha instance
#
#
define koha::instance (
  Enum['present', 'absent'] $ensure = 'present',
  Boolean $enabled = true,
  String $create_options = '--create-db',
  String $remove_options = '',
  Boolean $email_enabled = false,
  Boolean $plack_enabled = false,
  Boolean $rebuild_index = false,
  Boolean $create_superlibrarian = false,
  Optional[String] $sl_userid = undef,
  Optional[String] $sl_password = undef,
  Optional[String] $sl_categorycode = undef,
  Optional[String] $sl_branchcode = undef,
  Optional[Integer[1]] $sl_cardnumber = undef,
) {

  # The base class must be included first
  if !defined(Class['koha']) {
    fail('You must include the koha base class before using any koha defined resources')
  }

  if $ensure == 'present' {

    # Create instance using koha-create
    exec { "koha-create ${name}":
      command => "/usr/sbin/koha-create ${create_options} ${name}",
      creates => "${koha::etcdir}/sites/${name}",
    }

    # Create superlibrarian account
    # (only triggered along with koha-create)
    if $create_superlibrarian {

      # Validate superlibrarian parameters
      if empty($sl_userid) or empty($sl_password) or empty($sl_branchcode) or
        empty($sl_categorycode) or empty($sl_cardnumber) {
          fail('One or more missing parameters for superlibrarian')
      }

      $sl_params = @("EOT"/L)
                   --userid '${sl_userid}' \
                   --password '${sl_password}' \
                   --branchcode ${sl_branchcode} \
                   --categorycode ${sl_categorycode} \
                   --cardnumber ${sl_cardnumber}
                   | EOT

      exec { "create superlibrarian for ${name}":
        command     => "${koha::homedir}/bin/devel/create_superlibrarian.pl ${sl_params}",
        environment => [ "PERL5LIB=${koha::libdir}", "KOHA_CONF=${koha::etcdir}/sites/${name}/koha-conf.xml" ],
        refreshonly => true,
        subscribe   => Exec["koha-create ${name}"],
        require     => Exec["koha-create ${name}"],
      }
    }

    # Rebuild zebra index
    # (only triggered along with koha-create)
    if $rebuild_index {
      exec { "koha-rebuild-index for ${name}":
        command     => "/usr/bin/flock /run/koha/rebuild-index.lock /usr/sbin/koha-rebuild-zebra -q -f ${name} &",
        refreshonly => true,
        subscribe   => Exec["koha-create ${name}"],
        require     => Exec["koha-create ${name}"],
      }
    }

    # Manage instance enabled/disabled status
    if $enabled {
      exec { "koha-enable ${name}":
        command => "/usr/sbin/koha-enable ${name}",
        unless  => "/bin/bash -c 'source ${koha::homedir}/bin/koha-functions.sh && is_enabled ${name}'",
        require => Exec["koha-create ${name}"],
        tag     => 'restart-apache',
      }
    } else {
      exec { "koha-disable ${name}":
        command => "/usr/sbin/koha-disable ${name}",
        onlyif  => "/bin/bash -c 'source ${koha::homedir}/bin/koha-functions.sh && is_enabled ${name}'",
        require => Exec["koha-create ${name}"],
        tag     => 'restart-apache',
      }
    }

    # Manage instance Plack status
    if $plack_enabled {

      # Enable Plack in Apache2 site config
      exec { "koha-plack --enable ${name}":
        command => "/usr/sbin/koha-plack --enable ${name}",
        unless  => "/bin/bash -c 'source ${koha::homedir}/bin/koha-functions.sh && is_plack_enabled ${name}'",
        require => Exec["koha-create ${name}"],
        notify  => Exec["koha-plack --start ${name}"],
      }

      # Start Plack daemon
      exec { "koha-plack --start ${name}":
        command     => "/usr/sbin/koha-plack --start ${name}",
        refreshonly => true,
        tag         => 'restart-apache',
      }

      # Enable proxy_http module dependency for Apache2
      if $koha::manage_apache {
        include apache::mod::proxy
        include apache::mod::proxy_http
      }

    } else {

      # Disable Plack in Apache2 site config
      exec { "koha-plack --disable ${name}":
        command => "/usr/sbin/koha-plack --disable ${name}",
        onlyif  => "/bin/bash -c 'source ${koha::homedir}/bin/koha-functions.sh && is_plack_enabled ${name}'",
        require => Exec["koha-create ${name}"],
        notify  => Exec["koha-plack --stop ${name}"],
      }

      # Stop Plack daemon
      exec { "koha-plack --stop ${name}":
        command     => "/usr/sbin/koha-plack --stop ${name}",
        refreshonly => true,
        tag         => 'restart-apache',
      }
    }

    # Manage instance email status
    if $email_enabled  {
      exec { "koha-email-enable ${name}":
        command => "/usr/sbin/koha-email-enable ${name}",
        creates => "${koha::vardir}/${name}/email.enabled",
        require => Exec["koha-create ${name}"],
      }
    } else {
      exec { "koha-email-disable ${name}":
        command => "/usr/sbin/koha-email-disable ${name}",
        onlyif  => "test -f ${koha::vardir}/${name}/email.enabled",
        require => Exec["koha-create ${name}"],
      }
    }

  }
  else {

    # Remove instance using koha-remove
    exec { "koha-remove ${name}":
      command => "/usr/sbin/koha-remove ${remove_options} ${name}",
      onlyif  => "test -d ${koha::etcdir}/sites/${name}",
    }

  }

  # Restart webserver to load modified site config
  if $koha::manage_apache {
    Exec <| tag == 'restart-apache' |> {
      notify => Service['apache2']
    }
  }

}
